import React, { useEffect, useState } from "react";
import axios from "axios";
import { useSearchParams, useNavigate } from "react-router-dom";

export default function PeoplesPage() {
  const [people, setPeople] = useState([]);
  const [searchParams, setSearchParams] = useSearchParams();
  const [next, setNext] = useState();
  const [prev, setPrev] = useState();
  const [query, setQuery] = useState([]);
  const navigate = useNavigate();

  useEffect(() => {
    axios
      .get("https://swapi.dev/api/people?search=" + searchParams.get("search") + "&page=" + searchParams.get("page"))
      .then((res) => {
        setPeople(res.data.results);
        setNext(res.data.next);
        setPrev(res.data.previous);
      });
  }, []);

  var searchSomeone = (name) => {
    console.log("https://swapi.dev/api/people?search=" + name + "&page=" + searchParams.get("page"));
    axios.get("https://swapi.dev/api/people?search=" + name + "&page=" + searchParams.get("page")).then((res) => {
      setQuery(res.data.results);
      setNext(res.data.next);
      setPrev(res.data.prev);
    });
  };

  var handleNext = (next, searchParams) => {
    axios.get(next).then((res) => {
      setPeople(res.data.results);
      setNext(res.data.next);
      setPrev(res.data.previous);
    });
    setSearchParams({ search: searchParams.get("search"), page: parseInt(searchParams.get("page")) + 1 });
  };

  var handlePrev = (prev, searchParams) => {
    axios.get(prev).then((res) => {
      setPeople(res.data.results);
      setNext(res.data.next);
      setPrev(res.data.previous);
    });
    setSearchParams({ search: searchParams.get("search"), page: parseInt(searchParams.get("page")) - 1 });
  };
  var peopleCard = people.map((person) => {
    return (
      <div
        onClick={() => {
          var id = person.url.split("/")[5];
          console.log(id);
          navigate("/people/" + id);
        }}
        className="person-wrapper bg-slate-900 rounded-xl p-2 px-4 hover:bg-slate-600 md:w-max-1/2"
        key={person.name}
      >
        <div className="rounded">
          {/* <img
            className="rounded"
            src={"https://starwars-visualguide.com/assets/img/characters/" + index.toString() + ".jpg"}
            alt=""
          /> */}
        </div>
        <div className="text-white py-2">
          <p className="text-lg font-bold">{person.name}</p>
        </div>
      </div>
    );
  });

  if (query !== null) {
    var queryCard = query.map((person) => {
      return (
        <div className="person-wrapper bg-slate-900 rounded-xl p-2 px-4 hover:bg-slate-600" key={person.name}>
          <div className="rounded">
            {/* <img
            className="rounded"
            src={"https://starwars-visualguide.com/assets/img/characters/" + index.toString() + ".jpg"}
            alt=""
          /> */}
          </div>
          <div className="text-white py-2">
            <p className="text-lg font-bold">{person.name}</p>
          </div>
        </div>
      );
    });
  }

  return (
    <div className="container mx-auto p-2 m-5">
      <div className="flex align-center mb-3 justify-between">
        <h1 className="text-xl font-bold">Starwars People!</h1>
        <div className="flex justify-end ">
          <div className="mr-2">
            <input
              className="placeholder:italic placeholder:text-slate-400 block bg-white w-full border border-yellow-400 rounded-md py-2 pl-5 pr-3 shadow-sm focus:outline-none focus:border-sky-500 focus:ring-sky-500 focus:ring-1 sm:text-sm"
              placeholder="Search someone..."
              type="text"
              name="search"
              onInput={(e) => {
                setSearchParams({ search: e.target.value, page: 1 });
                searchSomeone(e.target.value);
                if (e.target.value === "") setSearchParams({ page: 1 });
              }}
            />
          </div>
          {next !== null ? (
            <button
              onClick={() => handleNext(next, searchParams)}
              className="px-4 py-1 rounded bg-yellow-500 font-semibold text-white mr-2 hover:scale-[1.02] hover:duration-300 hover:bg-yellow-600 cursor-pointer"
            >
              Next
            </button>
          ) : null}
          {prev !== null ? (
            <button
              onClick={() => handlePrev(prev, searchParams)}
              className="px-4 py-1 rounded bg-yellow-500 font-semibold text-white hover:scale-[1.02] hover:duration-300 hover:bg-yellow-600 cursor-pointer"
            >
              Previous
            </button>
          ) : null}
        </div>
      </div>
      {query.length == 0 ? (
        <div className="columns-1 gap-2 space-y-2 mb-4 md:columns-2">{peopleCard}</div>
      ) : (
        <div className="columns-1 gap-2 space-y-2 mb-4 md:columns-2">{queryCard}</div>
      )}
    </div>
  );
}
