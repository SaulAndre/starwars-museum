import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import axios from "axios";

export default function LandingPage() {
  const navigate = useNavigate();
  const [planets, setPlanets] = useState([]);
  useEffect(() => {
    axios.get("https://swapi.dev/api/planets").then((res) => {
      setPlanets(res.data.results);
    });
    // axios.get("https://swapi.dev/api/").then((res) => console.log(res));
  }, [planets]);
  var planetsCard = planets.map((planet) => {
    return (
      <div className="planet-wrapper border border-neutral-900 w-100 rounded mr-1" key={planet.name}>
        <p>{planet.name}</p>
        <p>{planet.rotation_period}</p>
        <p>{planet.diameter}</p>
        <p>{planet.climate}</p>
        <p>{planet.gravity}</p>
      </div>
    );
  });
  return (
    <div className="container mx-auto bg-gray-200 rounded-xl shadow border p-8 m-10">
      <h1 className="font-bold font-serif text-xl mb-5">Welcome To Starwars Museum</h1>
      <div className="menu-wrapper columns-1 gap-2 space-y-2 grid-flow-col md:columns-2 lg:columns-3">
        <div
          onClick={() => {
            navigate("/people?search=&page=1");
          }}
          className="mx-auto rounded justify-center align-middle relative AAAA "
        >
          <div className="bg-neutral-900 bg-opacity-75 text-white text-xl text-center font-bold w-full absolute bottom-0 py-1 md:py-2 lg:py-3">
            <h2 className="">People</h2>
          </div>
          <img className="rounded" src="https://media.timeout.com/images/105863223/750/422/image.jpg" alt="" />
        </div>
        <div
          onClick={() => navigate("/lala")}
          className="mx-auto rounded justify-center align-middle relative hover:scale-[1.02] hover:duration-300 cursor-pointer "
        >
          <div className="bg-neutral-900 bg-opacity-75 text-white text-xl text-center font-bold w-full absolute bottom-0 py-1 md:py-2 lg:py-3 ">
            <h2 className="">Films</h2>
          </div>
          <img
            className="rounded"
            src="https://socialjunkie.com/wp-content/uploads/2021/12/sgQUU6si9oqQzR63ePMssa.jpg"
            alt=""
          />
        </div>
        <div
          onClick={() => navigate("/lala")}
          className="mx-auto rounded justify-center align-middle relative hover:scale-[1.02] hover:duration-300 cursor-pointer "
        >
          <div className="bg-neutral-900 bg-opacity-75 text-white text-xl text-center font-bold w-full absolute bottom-0 py-1 md:py-2 lg:py-3 ">
            <h2 className="">Starships</h2>
          </div>
          <img
            className="rounded"
            src="https://dwgyu36up6iuz.cloudfront.net/heru80fdn/image/upload/c_fill,d_placeholder_wired.png,fl_progressive,g_face,h_450,q_80,w_800/v1576594418/wired_each-and-every-starfighter-in-star-wars-explained.jpg"
            alt=""
          />
        </div>
        <div
          onClick={() => navigate("/lala")}
          className="mx-auto rounded justify-center align-middle relative hover:scale-[1.02] hover:duration-300 cursor-pointer  "
        >
          <div className="bg-neutral-900 bg-opacity-75 text-white text-xl text-center font-bold w-full absolute bottom-0 py-1 md:py-2 lg:py-3 ">
            <h2 className="">Vehicles</h2>
          </div>
          <img
            className="rounded"
            src="https://www.gizmodo.com.au/wp-content/uploads/sites/2/2015/11/04/1503073127871649829.jpg"
            alt=""
          />
        </div>
        <div
          onClick={() => navigate("/lala")}
          className="mx-auto rounded justify-center align-middle relative hover:scale-[1.02] hover:duration-300 cursor-pointer "
        >
          <div className="bg-neutral-900 bg-opacity-75 text-white text-xl text-center font-bold w-full absolute bottom-0 py-1 md:py-2 lg:py-3 ">
            <h2 className="">Species</h2>
          </div>
          <img
            className="rounded"
            src="https://www.denofgeek.com/wp-content/uploads/2019/12/star-wars-alien-races.jpg?fit=1200%2C675"
            alt=""
          />
        </div>
        <div
          onClick={() => navigate("/lala")}
          className="mx-auto rounded justify-center align-middle relative hover:scale-[1.02] hover:duration-300 cursor-pointer "
        >
          <div className="bg-neutral-900 bg-opacity-75 text-white text-xl text-center font-bold w-full absolute bottom-0 py-1 md:py-2 lg:py-3 ">
            <h2 className="">Planets</h2>
          </div>
          <img
            className="rounded"
            src="https://dafunda.com/wp-content/uploads/2022/02/10-more-star-wars-planets-as-countries.jpg"
            alt=""
          />
        </div>
      </div>
      {/* <div className="buttons-wrapper flex">
        <button className="rounded-full bg-white px-3 py-1 mr-2 font-bold text-slate-500">People</button>
        <button className="rounded-full bg-white px-3 py-1 mr-2 font-bold text-slate-500">Films</button>
        <button className="rounded-full bg-white px-3 py-1 mr-2 font-bold text-slate-500">Starships</button>
        <button className="rounded-full bg-white px-3 py-1 mr-2 font-bold text-slate-500">Vehicles</button>
        <button className="rounded-full bg-white px-3 py-1 mr-2 font-bold text-slate-500">Species</button>
        <button className="rounded-full bg-white px-3 py-1 mr-2 font-bold text-slate-500">Planets</button>
      </div> */}
      {/* <input
        className="mb-2 placeholder:italic placeholder:text-slate-400 block bg-white w-full border border-slate-300 rounded-md py-2 pl-9 pr-3 shadow-sm focus:outline-none focus:border-sky-500 focus:ring-sky-500 focus:ring-1 sm:text-sm"
        placeholder="Search for anything..."
        type="text"
        name="search"
      /> */}
      {/* <div className="flex flex-wrap">{planetsCard}</div> */}
    </div>
  );
}
