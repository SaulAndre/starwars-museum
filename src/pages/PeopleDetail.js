import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import axios from "axios";

export default function PeopleDetail() {
  const [person, setPerson] = useState(null);
  const params = useParams();
  const [homeworld, setHomeworld] = useState(null);
  const [vehicles, setVehicles] = useState([]);
  const [films, setFilms] = useState([]);

  useEffect(() => {
    axios.get("https://swapi.dev/api/people/" + params.id).then((res) => {
      setPerson(res.data);
      axios.get(res.data.homeworld).then(async (res2) => {
        await setHomeworld(res2.data);
        await setFilms(res.data.films);
      });
    });
  }, []);
  if (person === null) {
    return (
      <div>
        <svg
          role="status"
          className="mr-2 w-8 h-8 text-gray-200 animate-spin dark:text-gray-600 fill-blue-600"
          viewBox="0 0 100 101"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            d="M100 50.5908C100 78.2051 77.6142 100.591 50 100.591C22.3858 100.591 0 78.2051 0 50.5908C0 22.9766 22.3858 0.59082 50 0.59082C77.6142 0.59082 100 22.9766 100 50.5908ZM9.08144 50.5908C9.08144 73.1895 27.4013 91.5094 50 91.5094C72.5987 91.5094 90.9186 73.1895 90.9186 50.5908C90.9186 27.9921 72.5987 9.67226 50 9.67226C27.4013 9.67226 9.08144 27.9921 9.08144 50.5908Z"
            fill="currentColor"
          ></path>
          <path
            d="M93.9676 39.0409C96.393 38.4038 97.8624 35.9116 97.0079 33.5539C95.2932 28.8227 92.871 24.3692 89.8167 20.348C85.8452 15.1192 80.8826 10.7238 75.2124 7.41289C69.5422 4.10194 63.2754 1.94025 56.7698 1.05124C51.7666 0.367541 46.6976 0.446843 41.7345 1.27873C39.2613 1.69328 37.813 4.19778 38.4501 6.62326C39.0873 9.04874 41.5694 10.4717 44.0505 10.1071C47.8511 9.54855 51.7191 9.52689 55.5402 10.0491C60.8642 10.7766 65.9928 12.5457 70.6331 15.2552C75.2735 17.9648 79.3347 21.5619 82.5849 25.841C84.9175 28.9121 86.7997 32.2913 88.1811 35.8758C89.083 38.2158 91.5421 39.6781 93.9676 39.0409Z"
            fill="currentFill"
          ></path>
        </svg>
      </div>
    );
  }
  return (
    <div className="container mx-auto p-2 m-5 flex gap-4">
      <div className="personal bg-slate-700 w-1/6 p-5 rounded-xl flex flex-col justify-center items-center">
        <div className="img-wrapper bg-slate-600 w-[200px] h-[200px] rounded-xl overflow-hidden mb-3">
          <img src="https://i.pravatar.cc/300" alt="" />
        </div>
        <div className="border-b-2 w-full text-center border-slate-600 pb-2">
          <h1 className="font-bold text-white text-xl">{person.name}</h1>
        </div>
        <div className=" py-2 text-white">
          <p>height: {person.height}</p>
          <p>birth year: {person.birth_year}</p>
          {homeworld !== null ? <p>homeworld: {homeworld.name}</p> : null}
        </div>
      </div>
      <div className="more-info bg-slate-600 w-1/3 p-5  rounded-xl">
        <div className="img-wrapper w-[200px] h-[200px]">
          <h2 className="text-white text-xl font-bold">Film List</h2>
          {/* {<Card films={films} />} */}
          {/* {vehicles.map((vehicle) => {
            return <div>{vehicle.name}</div>;
          })} */}
        </div>
      </div>
    </div>
  );
}

// function Card(props) {
//   var filmsList = props.films.map(async (film) => {
//     axios.get(film).then((res) => {
//       console.log(filmsList);
//       return (
//         <div className="border">
//           <p>{res.data.title}</p>
//           <p>{res.data.episode_id}</p>
//           <p>{res.data.director}</p>
//           <p>{res.data.producer}</p>
//         </div>
//       );
//     });
//   });
//   return <div> {filmsList}</div>;
// }
