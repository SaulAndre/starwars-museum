import React from "react";
import "./App.css";
import { BrowserRouter as Router, Routes, Route, Link } from "react-router-dom";
import LandingPage from "./pages/LandingPage";
import PeoplesPage from "./pages/PeoplesPage";
import PeopleDetail from "./pages/PeopleDetail";

// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyCtdkf78amzE4t8lptNg9_Cus1JhFKUk3s",
  authDomain: "newstarwars-38a61.firebaseapp.com",
  projectId: "newstarwars-38a61",
  storageBucket: "newstarwars-38a61.appspot.com",
  messagingSenderId: "784773028812",
  appId: "1:784773028812:web:46e5ea702529efd7425b69",
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

export default function App() {
  return (
    <Router>
      <Routes>
        <Route path="/people" element={<PeoplesPage />}></Route>
        <Route path="/people/:id" element={<PeopleDetail />}></Route>
        <Route path="/" element={<LandingPage />}></Route>
      </Routes>
    </Router>
  );
}
